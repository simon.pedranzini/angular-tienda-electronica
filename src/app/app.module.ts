import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

// Librerias
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

// Componentes
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/components/home/home.component';
import { ProductsComponent } from './pages/products/components/products/products.component';
import { StoresComponent } from './pages/stores/components/stores/stores.component';
import { ContactComponent } from './pages/contact/components/contact/contact.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { ProductDetailComponent } from './pages/products/components/product-detail/product-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ProductsComponent,
    StoresComponent,
    ContactComponent,
    NotFoundComponent,
    ProductDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
