import { Component } from '@angular/core';
import { faLocationArrow, faLaptop, faHome, faAt } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'angular-bootstrap';

  faLocationArrow = faLocationArrow;
  faLaptop = faLaptop;
  faHome = faHome;
  faAt = faAt;


}


