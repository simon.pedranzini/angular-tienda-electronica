import { ProductDetailComponent } from './pages/products/components/product-detail/product-detail.component';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './pages/contact/components/contact/contact.component';
import { HomeComponent } from './pages/home/components/home/home.component';
import { ProductsComponent } from './pages/products/components/products/products.component';
import { StoresComponent } from './pages/stores/components/stores/stores.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const routes: Routes = [
  { path: 'inicio', component: HomeComponent},
  { path: 'productos', component: ProductsComponent },
  { path: 'productos/:productoId', component: ProductDetailComponent},
  { path: 'tiendas', component: StoresComponent },
  { path: 'contacto', component: ContactComponent},
  { path: '', redirectTo: 'inicio', pathMatch: 'full'},
  { path: '**', component: NotFoundComponent} // equivalente al 404 not found, otra pagina/componente
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
