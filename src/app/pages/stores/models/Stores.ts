export interface Stores {
  ciudad: string,
  sede: Infosede,
  img: string
}

// export interface Sedes {
//   sede_uno: Infosede,
//   sede_dos?: Infosede,
//   sede_tres?: Infosede,
// }

export interface Infosede {
  direccion: string,
  codigoPostal: number,
  provincia: string,
}

