import { Login } from './../../models/acces';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit  {

  public signForm : FormGroup;

constructor(private fb : FormBuilder) {
  this.signForm = this.fb.group ({
    email: ['', [Validators.required, Validators.email]],
    password: ['', [Validators.required, Validators.minLength(8)]]
  },
  // { validators: comparePassword('contra1', 'contra2') } // Validadores customizados
  // Esto va en otro fichero, donde hace todo el tinglado para comparar los dos valores.
  )
 }

ngOnInit(): void {
}

  onSubmit() {
    if (this.signForm.valid) {
      console.log('Datos Correcto. Enviando Formulario')
    } else {
      console.log('Datos incorrectos. Revisalo')
      // this.signForm.markAllAsTouched;
    }
    }

    get emailField() {
      return this.signForm.get('email')
    }
    get passwordField() {
      return this.signForm.get('password')
    }
  }

  // AQUI ME HAYO, SIN PODER MOSTRAR LOS ERRORES POR PANTALLA
