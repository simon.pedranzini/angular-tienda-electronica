export interface Items {
  id: number;
  name: string;
  category: 'Movil' | 'Ordenador' | 'Tableta' | 'Portatil' | 'Consola';
  img: string;
  // arrayNumber: Numeros[]; Para poder un array de numeros
}

// export interface Numeros {
//   matricula: number;
// }

