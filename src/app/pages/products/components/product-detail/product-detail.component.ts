import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { globalProducts } from '../products/products.component';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit {

  prodId?: string;
  productDetail?: any = {};
  paramMapSubscription?: Subscription;

  constructor(private routes: ActivatedRoute) { }

  ngOnInit(): void { // PRIMERA FUNCION QUE SE EJECUTA AL CARGAR

    this.paramMapSubscription = this.routes.paramMap //PARAMMAP, listado de parametros
      .subscribe(data => {
        this.prodId = data.get("productoId") || ''; //productId el que recibe por url, del path routing.

        //MOSTRARLO EN EL HTML
          this.productDetail = globalProducts.find(product => product.id.toString() === this.prodId)
      });

  }

  ngOnDestroy() {
    this.paramMapSubscription?.unsubscribe();
  }

}
