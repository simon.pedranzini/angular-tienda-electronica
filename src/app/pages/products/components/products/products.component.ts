import { Items } from "./../../models/Items";
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})


export class ProductsComponent implements OnInit { //Cuando se carga el listado de productos

// LA "BBDD" SE INICIALIZA FUERA DEL PRODUCTSCOMPONENT!
params : any;
products = globalProducts;


  constructor(private routes: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.routes.params.subscribe( params => {
      this.params = params;
    })
  }

}

export const  globalProducts = [{
  id: 12659,
  name: 'MacBook Pro',
  category: 'Portatil',
  img: './assets/macbook-pro.png'
},
{
  id: 45873,
  name: 'MacBook Air',
  category: 'Portatil',
  img: './assets/macbook-air.png'
},
{
  id: 46859,
  name: 'Samsung Galaxy A8',
  category: 'Movil', // Si está mal escrito te avisa.
  img: './assets/samsung-galaxy-a8.png'
},
{
  id: 23458,
  name: 'iPhone 13 Pro',
  category: 'Movil',
  img: './assets/iphone-13-pro.png'
},
{
  id: 42189,
  name: 'PlayStation 5',
  category: 'Consola',
  img: './assets/playstation-5.png'
},
{
  id: 36669,
  name: 'HP Intel Gaming',
  category: 'Ordenador',
  img: './assets/hp-intel-gaming.png'
},
{
id: 47888,
name: 'Ipad Pro',
category: 'Tableta',
img: './assets/ipad-pro.png'
}
]
